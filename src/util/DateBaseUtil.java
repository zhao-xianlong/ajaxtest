package util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

public class DateBaseUtil {
    //1.定义需要的工具类对象
    protected Connection connection;
    protected PreparedStatement pps;
    protected ResultSet rs;
    protected int k = 0;//受影响的行数

    private static String url;
    private static String username;
    private static String password;
    private static String dirverName;

    //2.加载驱动
    static {
        try {
            /*InputStream inputStream = DateBaseUtil.class.getClassLoader()
                .getResourceAsStream("db.properties");

            Properties properties = new Properties();
            properties.load(inputStream);

            dirverName = properties.getProperty("driverclass");
            url = properties.getProperty("url");
            username = properties.getProperty("uname");
            password = properties.getProperty("upass");*/
            ResourceBundle bundle = ResourceBundle.getBundle("db");
            dirverName = bundle.getString("driverclass");
            url = bundle.getString("url");
            username = bundle.getString("uname");
            password = bundle.getString("upass");

            Class.forName(dirverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }/* catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    //3.获得连接
    protected Connection getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }

    //4.创建通道
    protected PreparedStatement getPps(String sql) {
        try {
            pps = getConnection().prepareStatement(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return pps;
    }

    //5.给占位符赋值 list中保存的是给占位符所赋的值
    private void setParams(List list) {
        try {
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    pps.setObject(i + 1, list.get(i));
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //6.增删改调取的方法
    protected int update(String sql, List params) {
        try {
            getPps(sql);
            setParams(params);
            k = pps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return k;
    }

    //7.查询的时候调取一个方法
    protected ResultSet query(String sql, List list) {
        try {
            getPps(sql);
            setParams(list);
            rs = pps.executeQuery();
            return rs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    //8.关闭资源
    protected void closeall() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (pps != null) {
                pps.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
