package servlet;

import bean.Users;
import dao.impl.UsersDaoImpl;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/getuser")
public class GetUsersServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收参数
        String uid = req.getParameter("uid");
        int userid=Integer.parseInt(uid);

        //2.从数据库查询得到user对象
        UsersDaoImpl usersDao = new UsersDaoImpl();
        Users users = usersDao.getUsers(userid);

        //java->json
        JSONObject jsonObject = JSONObject.fromObject(users);//json

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.print(jsonObject);

    }
}
