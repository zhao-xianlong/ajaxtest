package dao;

import bean.Users;

public interface UsersDao {
    public boolean verifyUserNameValidity(String uname);
    public Users getUsers(Integer uid);
}
