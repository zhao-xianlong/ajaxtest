package dao.impl;

import bean.Users;
import dao.UsersDao;
import util.DateBaseUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersDaoImpl extends DateBaseUtil implements UsersDao {

    @Override
    public boolean verifyUserNameValidity(String uname) {
        String sql = "select * from users where username = ?";
        ArrayList list = new ArrayList();
        list.add(uname);
        ResultSet rs = query(sql, list);
        try {
            if(rs.next()) return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            closeall();
        }
        return false;
    }

    @Override
    public Users getUsers(Integer uid) {
        Users users = new Users();
        ArrayList list = new ArrayList();
        try {
            String sql = "select * from users where uid = ?";
            list.add(uid);
            ResultSet rs = query(sql, list);
            while (rs.next()){
                users.setUid(rs.getInt("uid"));
                users.setUsername(rs.getString("username"));
                users.setPassword(rs.getString("password"));
                users.setMoney(rs.getDouble("money"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            closeall();
        }
        return users;
    }

}
