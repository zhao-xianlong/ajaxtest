package bean;

public class Users {
    private Integer uid;
    private String username;
    private String password;
    private double money;

    public Users() {
    }

    public Users(Integer uid, String username, String password, double money) {
        this.uid = uid;
        this.username = username;
        this.password = password;
        this.money = money;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
